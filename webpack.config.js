
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    plugins: [
        new MiniCssExtractPlugin({ filename: 'mithril-design.min.css' })
    ],
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: [ MiniCssExtractPlugin.loader, 'css-loader' ],
            }
        ]
    },
    output: {
        path: __dirname + '/dist',
        filename: 'mithril-design.min.js',
        library: 'md',
        publicPath: '/'
    },
    devServer: {
        historyApiFallback: true,
    },
    externals: [
        'mithril'
    ]
};
