# Mithril Design Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.3.2] - 2021-04-01

### Added
- infinite scroll feature to list component
- support for array of button options besides map of actions in app bar actions

### Fixed
- other elements being actioned when clicking outside an open menu to close it
- nav drawer not closing upon click on scrim when scrim is wider than 56px
- menu component to always close after item click
- menu fade in to originate from right when repositioned to the left
- spec implementation of the transitions for menu and dialog components
- menus being displayed below app bar
- dialog sheet having too much rounding on the corners
- list global action to be always run when defined
- all relevant scrolling event listeners to be passive

## [v0.3.1] - 2021-03-25

### Fixed
- icon buttons ripple inconsistency to spec
- numeric field ignoring initial bound value
- page sides-crolling when  content overflows horizontally
- list and button component waiting too long for ripple before action
- not found route denifition
- list item avatar not constraining image proportion
- tab switching stutter
- tab indicator not moving along with drag
- tab switching transition too slow

## [v0.3.0] - 2021-03-23

### Added
- `title` option to app
- feature to set page title to current view title plus app title
- option to "close" app-bar button to execute a custom function
- proper rippling effects to buttons, lists items and cards
- missing material design icons

### Fixed
- height of single line list items with avatar
- icon button to not display ripple effect
- menu and list global action interface
- snackbar styling to live up to the spec
- snackbar animation when FAB is on screen
- FAB not being rendered on first view appearance
- card elements styling

### Changed
- interface of `Tabs` to receive children
- card action interface
- menu button interface to expose only items

### Removed
- `TabPanel` component in favor of `Tabs` children

## [v0.2.2] - 2021-03-18

### Added
- overline feature to list items
- button feature to open a menu on click
- missing hr styling for list component
- contained icon button styling (smaller FAB)

### Fixed
- list to not render falsey items
- dialog not being draw sometimes upon calling

## [v0.2.1] - 2021-03-16

### Fixed
- details in general text styling
- snackbar behavior and apperance according to spec

## [v0.2.0] - 2021-03-12

### Added
- chip list component
- search feature to app bar component
- throttle feature to text-field update event

### Fixed
- list item first line styling to show ellipsis
- animation of list item action

### Changed
- floating menu interface and mechanics

### Fixed
- nav drawer width math to better conform to spec

## [v0.1.2] - 2021-03-12

### Added
- custom validation feature to form fields

### Fixed
- low visibility of text field label on error state

## [v0.1.1] - 2021-03-11

### Fixed
- fatal broken reference to chips file
- relying on Roboto font existing on the client OS

## [v0.1.0] - 2021-03-11

### Changed
- FAB interface to fix view glitches
- `AppBar` interface to be used through `view()`
- WebPack version to 5.x

### Fixed
- App Bar scrolling behavior
- tree-shaking of components
- tree-shaking of CSS

## [v0.0.10] - 2020-12-23

### Added
- `TimeField` component
- support for `classes` on list component
- loading pattern to form component

### Changed
- app first render to delay until window load event

## [v0.0.9] - 2020-12-21

### Added
- `onchange` hook to tab bar
- missing link to google Roboto font
- support to two line list items

### Changed
- FAB and Snackbar to use new floating stack approach

### Fixed
- App Bar scrolling behavior
- list styling

## [v0.0.8] - 2020-12-15

### Changed
- spacing details on card component

## [v0.0.7] - 2020-12-14

### Fixed
- buggy scrolling behavior when using tabs

## [v0.0.6] - 2020-12-14

### Changed
- snackbar text color for better contrast

### Fixed
- blocking button page loader being show after action is carried

## [v0.0.5] - 2020-12-13

### Added
- `FAB` component
- `m.app()` for defining your app basics
- `m.view()` for creating a component that is a full-fledged view
- `NavDrawer` component
- `Tabs` and `TabComponent` components
- support for close button on the App Bar.
- Progress meter components
- `Form` component
- `m.snackBar()` for triggering snack bars
- `m.dialog()` and `m.confirm()`
- `List` component

### Changed
- behavior of app rounting to use new view concept
- `Dropdown` component to be properly functional
- `NumberField` component to be properly functional

### Removed
- `IconButton` component in favor of `icon` attribute on `Button` component

## [v0.0.4] - 2020-07-13

### Changed
- dropdown input data interface

### Fixed
- dropdown bug where selected text was not being shown

## [v0.0.3] - 2020-07-13

### Fixed
- app bar scrolling behavior to properly stick when near to the top of the page

## [v0.0.2] - 2020-07-11

### Fixed
- minor styling quirks

### Changed
- the way data is read by input components

## [v0.0.1] - 2020-05-29

### Fixed
- bug in chips input where separator characters were allowed as first character in a chip

## [v0.0.0] - 2020-05-26
First officially published version

[v0.0.0]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.0
[v0.0.1]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.1
[v0.0.2]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.2
[v0.0.3]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.3
[v0.0.4]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.4
[v0.0.5]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.5
[v0.0.6]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.6
[v0.0.7]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.7
[v0.0.8]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.8
[v0.0.9]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.9
[v0.0.10]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.0.10
[v0.1.0]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.1.0
[v0.1.1]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.1.1
[v0.1.2]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.1.2
[v0.2.0]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.2.0
[v0.2.1]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.2.1
[v0.2.2]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.2.2
[v0.3.0]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.3.0
[v0.3.1]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.3.1
[v0.3.2]: https://gitlab.com/GCSBOSS/mithril-design/-/tags/v0.3.2
