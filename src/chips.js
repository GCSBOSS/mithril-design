
import m from 'mithril';
import { Button } from './button';
import { parseClasses } from './util';
import './chips.css';

export function Chip(){

    return {

        view(vnode){
            let text = vnode.attrs.text || 'Chip';

            return m('article.chip', [
                text,

                typeof vnode.attrs.onClose == 'function' &&
                    m(Button, { icon: 'cancel', action: vnode.attrs.onClose })
            ]);
        }

    };
}

export function ChipList(){

    return {

        view(vnode){
            return m('section.chips', vnode.attrs.items.map(d => m(Chip, d)));
        }

    }

}

export function ChipsField(){

    let index = {};
    let hostForm = {};
    let name;

    function removeChip(vnode, n = false){
        n = n === false ? hostForm[name].length - 1 : n;
        let text = hostForm[name].splice(n, 1)[0];
        delete index[text];

        if(typeof vnode.attrs.onRemove == 'function')
            vnode.attrs.onRemove(text, n);
    }

    function addChip(vnode, text){
        if(text in index)
            return;

        text = text.trim();
        hostForm[name].push(text);
        index[text] = true;

        if(typeof vnode.attrs.onAdd == 'function')
            vnode.attrs.onAdd(text);
    }

    return {

        view(vnode){
            if(vnode.attrs.error instanceof Error)
                vnode.attrs.error = vnode.attrs.error.message;

            let attrs = {};
            attrs.id = vnode.attrs.id;

            let dc = vnode.attrs.disabled ? 'disabled' : '';
            let oc = vnode.attrs.outlined ? 'outline' : '';
            let ec = vnode.attrs.error ? 'error' : '';

            if(typeof vnode.attrs.bind == 'object')
                hostForm = vnode.attrs.bind;
            name = vnode.attrs.name || 'chips';
            !Array.isArray(hostForm[name]) && (hostForm[name] = []);

            let req = vnode.attrs.req ? '*' : '';

            return m('label.text-field.chips' + parseClasses(vnode.attrs.classes, dc, oc, ec),
                attrs,

                (vnode.attrs.helper || vnode.attrs.error)
                    && m('span.helper', vnode.attrs.error || vnode.attrs.helper),

                m('div',
                    m('input', { type: 'text', placeholder: vnode.attrs.placeholder,
                        disabled: Boolean(dc) }),
                    hostForm[name].map((i, n) =>
                        m(Chip, { text: i, onClose: () => removeChip(vnode, n) }))
                ),

                m(
                    'select[hidden][multiple]',
                    { name, disabled: Boolean(dc) },
                    hostForm[name].map(i =>
                        m('option[selected]', { value: i.trim() }))
                ),

                m('span', (vnode.attrs.label || 'Chips Field') + req)
            );
        },

        oncreate(vnode){

            let seps = new RegExp('[' + (vnode.attrs.sep || ',;') + ']');

            vnode.dom.addEventListener('beforeinput', function(e){
                let input = vnode.dom.control;

                if(e.inputType == 'insertLineBreak' || seps.test(e.data)){
                    if(input.value.length < 1){
                        e.preventDefault();
                        return false;
                    }
                    addChip(vnode, input.value);
                    input.focus();
                    input.value = '';
                    e.preventDefault();
                    m.redraw();
                    return false;
                }

                if(hostForm[name].length > 0 && input.value.length == 0 && e.inputType == 'deleteContentBackward'){
                    removeChip(vnode);
                    m.redraw();
                }

            });

            vnode.dom.addEventListener('focusout', function(){
                if(vnode.dom.control.value.length > 0){
                    addChip(vnode, vnode.dom.control.value);
                    vnode.dom.control.value = '';
                    m.redraw();
                }
            });
        }

    };
}
