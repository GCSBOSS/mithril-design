
import m from 'mithril';
import { parseClasses, filterAttrs, globalState } from './util';
import { fab, FAB } from './button';
import { Dialog } from './dialog';
import { Menu } from './menu';
import { AppBar } from './app-bar';
import { SnackBar } from './snack-bar';
import { BlockingLoader } from './progress';
import './view.css';

export const standardEasing = 'cubic-bezier(0.4, 0.0, 0.2, 1)';

async function animateViewOut(vnode){

    if(globalState.nav == 'top'){
        let a = vnode.dom.animate([
            { 'z-index': '-1', opacity: 1 },
            { 'z-index': '-1', opacity: .4 }
        ], { duration: 90, easing: standardEasing });

        await new Promise(done => a.onfinish = done);
    }
}

async function animateViewIn(vnode){

    if(globalState.nav == 'top'){
        let a = vnode.dom.animate([
            { 'z-index': '1', opacity: 0, transform: 'scale(.92)' },
            { 'z-index': '1', opacity: 1, transform: 'scale(1)' }
        ], { duration: 210, easing: standardEasing });
        await new Promise(done => a.onfinish = done);
    }

}

export function view(id, pojo){
    let { view: v, onbeforeremove, oncreate } = pojo;
    pojo.view = vnode => m(View, { id, appBar: pojo.appBar }, v(vnode));
    pojo.oncreate = async vnode => {
        await animateViewIn(vnode);
        typeof oncreate == 'function' && oncreate(vnode);
    };
    pojo.onbeforeremove = async vnode => {
        typeof onbeforeremove == 'function' && await onbeforeremove(vnode);
        await animateViewOut(vnode);
    };
    return pojo;
}

export function View(){

    return {

        oninit(){
            fab(false);
        },

        oncreate(){
            document.body.scrollTop = 0;
            if(globalState.appTitle)
                document.title = globalState.appTitle +
                    (typeof globalState.pageTitle == 'string'
                        ? ' - ' + globalState.pageTitle
                        : '');
        },

        onupdate(){
            document.body.scrollTop = 0;
            if(globalState.appTitle)
                document.title = globalState.appTitle +
                    (typeof globalState.pageTitle == 'string'
                        ? ' - ' + globalState.pageTitle
                        : '');
        },

        onbeforeremove(){
            globalState.pageTitle = false;
        },

        view(vnode){
            let attrs = filterAttrs(vnode.attrs);

            attrs.onscroll = function(ev){
                if(ev.target.scrollHeight > window.innerHeight
                && ev.target.scrollTop + 900 >= ev.target.scrollHeight){
                    let event = new Event('scrollmore', { bubbles: false });
                    for(let el of ev.target.children)
                        el.dispatchEvent(event);
                }
            }

            return [
                vnode.attrs.appBar && m(AppBar, vnode.attrs.appBar),
                m('main' + parseClasses(vnode.attrs.classes), attrs, vnode.children),
                globalState.navDrawerComponent && m(globalState.navDrawerComponent),
                m('aside#bottom-stack', [
                    globalState.snackBar && m(SnackBar, globalState.snackBar),
                    globalState.fab && m(FAB, globalState.fab),
                ]),
                globalState.contextMenu && m(Menu, globalState.contextMenu),
                globalState.blockingLoader && m(BlockingLoader),
                globalState.dialogOptions && m(Dialog, globalState.dialogOptions)
            ];
        }

    };

}
