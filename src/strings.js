
export default {
    RequiredFieldMessage: 'This field is required',
    SearchBarPlaceholder: 'Search...'
}
