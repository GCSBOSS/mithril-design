
import m from 'mithril';
import './progress.css';

async function FadeIn(vnode){
    let a = vnode.dom.animate([
        { opacity: '0' },
        { opacity: '1' }
    ]);
    await new Promise(done => a.onfinish = done);
}

async function FadeOut(vnode){
    let a = vnode.dom.animate([
        { opacity: '1' },
        { opacity: '0' }
    ]);
    await new Promise(done => a.onfinish = done);
}


export function Loader(){
    return {
        view(){
            return m('progress.circular.indeterminate');
        }
    };
}

export function LinearProgress(){
    return {
        view(){
            return m('progress.linear.indeterminate');
        }
    };
}

export function BlockingLoader(){
    return {
        onbeforeremove: FadeOut,
        oncreate: FadeIn,
        view(){
            return m('div.blocking', m(Loader));
        }
    };
}
