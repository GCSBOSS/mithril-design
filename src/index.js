import './index.css';
// import './theme-light.css';

import m from 'mithril';
export { TextField, RedactedField, NumericField, TimeField } from './text-field';
export { Button, FAB, fab } from './button';
export { Loader, BlockingLoader, LinearProgress } from './progress';
export { Form } from './form';
export { AppBar } from './app-bar';
export { Card } from './card';
export { Tabs /*, tab*/ } from './tabs';
export { Menu } from './menu';
export { List } from './list';
export { Dropdown } from './dropdown';
export { SnackBar, snackBar } from './snack-bar';
export { NavDrawer } from './nav-drawer';
export { Chip, ChipsField, ChipList } from './chips';
export { confirm, Dialog, dialog } from './dialog';
// import Strings from './strings';

import { view } from './view';
import { app, globalState } from './util';

Object.assign(m, { view, app, worker: () => globalState.worker });

export default m;
