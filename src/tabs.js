

import m from 'mithril';
import { globalState, animate } from './util';
import { Button } from './button';
import './tabs.css';

globalState.tabs = {};

export function Tabs(){

    let winWid, name, curIndex, deltaY = 0, deltaX = 0, tsX, tsY, state = false, itemCount;

    function switchTabWithClick(ev, index){

        ev.target.parentNode.style.setProperty('--tab-indicator-width', ev.target.offsetWidth + 'px');
        ev.target.parentNode.style.setProperty('--tab-indicator-x', ev.target.offsetLeft + 'px');

        let initialLeft = -index * winWid;
        let elem = ev.target.parentNode;
        let idx = 0;
        while(elem = elem.nextElementSibling){
            animate(elem, [
                { left: String((-curIndex + idx) * winWid + deltaX) + 'px' },
                { left: initialLeft + 'px' }
            ], { duration: 160 })
            initialLeft += winWid;
            idx++;
        }

        deltaX = 0;

        setTimeout(() => {
            globalState.tabs[name].active = index;
            m.redraw();
        }, 160);
    }

    function ontouchmove(ev){
        let t = ev.changedTouches[0];

        deltaY = t.screenY - tsY;
        deltaX = t.screenX - tsX;

        // Ensure scrolling and dragging don't happen simultaneously (railing)
        let ax = Math.abs(deltaX);
        if(!state){
            let ay = Math.abs(deltaY);

            // Prevent first and last elements to be dragged inwards
            let draggingPastFirst = curIndex == 0 && deltaX > 0;
            let draggingPastLast = curIndex == itemCount - 1 && deltaX < 0;

            if(ax > 8 && ax > ay && !draggingPastFirst && !draggingPastLast)
                state = 'dragging';
            else if(ay > 5 && ay > ax)
                state = 'scrolling';

            ev.preventDefault();
            deltaX = 0;
        }
        else if(state == 'dragging'){
            ev.preventDefault();

            // Slide and resize tab indicator
            let ratio = deltaX / winWid;
            let willIndex = curIndex - (ratio > 0 ? Math.ceil(ratio) : Math.floor(ratio));

            let tabs = document.querySelector('nav.tabs');
            let oldBtn = tabs.children[curIndex];
            let newBtn = tabs.children[willIndex];

            try{
                let difWid = oldBtn.offsetWidth - newBtn.offsetWidth;
                tabs.style.setProperty('--tab-indicator-width',
                    String(oldBtn.offsetWidth - difWid * ratio) + 'px');
                let difLeft = oldBtn.offsetLeft - newBtn.offsetLeft;
                tabs.style.setProperty('--tab-indicator-x',
                    String(oldBtn.offsetLeft - difLeft * Math.abs(ratio)) + 'px');
            }
            catch(e){}
        }
        else if(state == 'scrolling')
            deltaX = 0;
    }

    function ontouchstart(ev){
        let t = ev.touches[0];
        tsX = t.screenX;
        tsY = t.screenY;
    }

    function ontouchend(){
        state = false;
        let newIndex = globalState.tabs[name].active - Math.round(deltaX / winWid);
        let target = document.querySelector('nav.tabs').children[newIndex];
        switchTabWithClick({ target }, newIndex);
    }

    return {

        oninit(vnode){
            winWid = window.innerWidth;

            let index = vnode.attrs.initial || 0;
            globalState.tabs[vnode.attrs.name || 'tabs'] = { active: index };
            if(typeof vnode.attrs.onchange == 'function')
                vnode.attrs.onchange(index);
        },

        view(vnode){
            name = vnode.attrs.name || 'tabs';
            let active = globalState.tabs[name].active;
            let changed = curIndex != active;
            itemCount = vnode.attrs.items.length;
            curIndex = active;

            let btns = [];
            for(let index in vnode.attrs.items)
                btns.push(m(Button, {
                    type: 'contained',
                    text: vnode.attrs.items[index],
                    classes: index == curIndex ? 'active' : '',
                    action: ev => switchTabWithClick(ev, index)
                }));

            if(changed && typeof vnode.attrs.onchange == 'function')
                vnode.attrs.onchange(curIndex);

            return [
                m('nav.tabs', btns),
                vnode.children.map((cvn, idx) =>
                    m('article#tab' + idx + (idx == curIndex ? '.active' : ''),
                        { ontouchstart, ontouchend, ontouchmove, style: {
                            left: String((-curIndex + idx) * winWid + deltaX) + 'px'
                        } }, cvn))
            ];
        },

        oncreate(vnode){
            let activeBtn = vnode.dom.querySelector('.active');
            vnode.dom.style.setProperty('--tab-indicator-width', activeBtn.offsetWidth + 'px');
            vnode.dom.style.setProperty('--tab-indicator-x', activeBtn.offsetLeft + 'px');
        }

    };
}
//
// export function tab(name, index){
//     if(!globalState.tabs[name])
//         return;
//     globalState.tabs[name].active = index;
//     m.redraw();
// }
