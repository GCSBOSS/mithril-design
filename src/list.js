
import m from 'mithril';
import { parseClasses, filterAttrs, clickRipple, delay } from './util';
import './list.css';

export function ListItem(){

    return {

        /*async onbeforeremove(vnode){
            await animate(vnode.dom, [
                {  },
                { opacity: 0 }
            ], { duration: 1000 });
        },*/

        view(vnode){

            if(vnode.children[0] == 'hr')
                return m('hr');

            let i = vnode.attrs;
            let cls = i.classes || '';
            let attrs = {};
            if(i.key)
                attrs.key = i.key;

            i = typeof i == 'string' ? { text: i } : i;

            if(i.img)
                cls += '.avatar';

            if(i.overline){
                i.text = [ i.text, i.overline ];
                cls += '.overline';
            }

            if(Array.isArray(i.text))
                cls += '.lines';

            if(i.icon)
                attrs['data-icon-pre'] = i.icon;

            if(i.value)
                attrs['data-value'] = i.value;

            attrs.onclick = async function(ev){
                if(ev.target.tagName == 'BUTTON')
                    return;

                if(typeof i.action == 'function' || typeof i.globalAction == 'function'){
                    clickRipple(this, ev);
                    await delay(140);
                }

                if(typeof i.action == 'function')
                    i.action(ev);
                if(typeof i.globalAction == 'function')
                    i.globalAction(ev);

                m.redraw();
            };

            return m('li' + cls + parseClasses(vnode.attrs.classes), attrs,
                i.img && m('img', { src: i.img, alt: '' }),
                typeof i.text == 'string' && m('label', i.text),
                Array.isArray(i.text) && [
                    m('label', i.text[0]),
                    m('span', ...i.text.slice(1)),
                ],
                i.after
            );
        }

    };

}

export function List(){

    let scrollMoreFn, loadingMore = false, items, page = 1, noMore = false;

    async function onScrollMore(){
        if(loadingMore || noMore)
            return;
        page++;
        loadingMore = true;
        let r = await scrollMoreFn(page);
        loadingMore = false;
        if(r.length == 0)
            noMore = true;
    }

    return {

        oncreate(vnode){
            if(typeof scrollMoreFn == 'function')
                vnode.dom.addEventListener('scrollmore', onScrollMore);
        },

        onupdate(vnode){
            vnode.dom.removeEventListener('scrollmore', onScrollMore);
            if(typeof scrollMoreFn == 'function')
                vnode.dom.addEventListener('scrollmore', onScrollMore);
        },

        view(vnode){
            let attrs = filterAttrs(vnode.attrs, 'style');
            let cls = parseClasses(vnode.attrs.classes);

            scrollMoreFn = vnode.attrs.onScrollMore;
            let oldLength = items ? items.length : 0;
            items = vnode.attrs.items.filter(a => a);
            if(oldLength > (items ? items.length : 0)){
                page = 1;
                noMore = false;
            }

            return m('ul' + cls, attrs, items.map(i => {
                if(i && typeof vnode.attrs.action == 'function')
                    i.globalAction = vnode.attrs.action;
                return m(ListItem, i);
            }));
        }

    };
}
