import m from 'mithril';
import { globalState, delay, fadeIn, fadeOut, filterAttrs, parseClasses } from './util';
import { List } from './list';
import './menu.css';

export function Menu(){

    let left = 0, top = 0, width = false, globalAction;

    function globalClick(ev){
        setTimeout(() => menu(false), 300);
        if(typeof globalAction == 'function')
            globalAction(ev.target.dataset.value);
    }

    return {

        oninit(vnode){
            left = vnode.attrs.left;
            top = vnode.attrs.top;
            width = vnode.attrs.width;
        },

        view(vnode){
            let style = { left: left + 'px', top: top + 'px' };
            if(width)
                style.width = width + 'px';

            globalAction = vnode.attrs.action;
            let items = vnode.attrs.items;

            if(typeof items !== 'object')
                items = [];
            else if(!Array.isArray(items))
                items = Object.keys(items).map(k => ({ text: k, action: items[k] }));

            // data.length == 0 ? m('span', vnode.attrs.placeholder) : items
            let attrs = filterAttrs(vnode.attrs, 'rowId', 'rowName');
            return m(List, { ...attrs, action: globalClick, style, items,
                classes: '.menu' + parseClasses(vnode.attrs.classes) });
        },

        oncreate(vnode){
            let { top, left, width, right, bottom } = vnode.dom.getBoundingClientRect();
            vnode.dom.style.maxHeight = String(window.innerHeight - top - 8) + 'px';

            if(left + width + 8 > window.innerWidth)
                vnode.dom.style.left = String(window.innerWidth - width - 8) + 'px';

            if(left + width / 2 + 8 > window.innerWidth)
                vnode.dom.style.transformOrigin = 'top right';

            vnode.dom.addEventListener('click', function(ev){
                if(ev.clientX < left || ev.clientX > right || ev.clientY < top
                || ev.clientY > bottom)
                    menu(false);
            });

            fadeIn(vnode);
        },

        async onbeforeremove(vnode){
            await fadeOut(vnode);
        },

        onupdate(vnode){
            let { top, width, left } = vnode.dom.getBoundingClientRect();

            if(left + width + 8 > window.innerWidth)
                vnode.dom.style.left = String(window.innerWidth - width - 8) + 'px';

            if(left + width / 2 + 8 > window.innerWidth)
                vnode.dom.style.transformOrigin = 'top right';

            vnode.dom.style.maxHeight = String(window.innerHeight - top - 8) + 'px';
        }

    };
}

export async function menu(opts){
    globalState.contextMenu = false;
    m.redraw();
    await delay(100);
    if(opts){
        globalState.contextMenu = opts;
        m.redraw();
    }
}
