
import m from 'mithril';
import { Button } from './button';
import { TextField } from './text-field';
import Strings from './strings';
import { parseClasses, globalState } from './util';
import './app-bar.css';

export function AppBar(){

    // TODO  prevent scrolling of element while reducing

    let barShowing = true;
    let topOnHold = 0, top = 0, dragging = false, pointZero, prevTop = 0;
    let openSearch = false;
    let bindSearch = {};

    window.addEventListener('touchstart', function(ev){
        for(var el of ev.path)
            if(el.nodeName == '#document' || el.tagName == 'MAIN' || el.classList && el.classList.contains('active') && el.tagName == 'ARTICLE'){
                pointZero = el.scrollTop;
                topOnHold = barShowing ? 0 : -56;

                m.redraw();
                break;
            }
    }, { passive: true });

    window.addEventListener('scroll', function(ev){
        let el = ev.target;

        if(el.nodeName == '#document')
            el = document.body.firstElementChild;
        else if(el.tagName != 'MAIN' && (el.classList && !el.classList.contains('active') || el.tagName != 'ARTICLE'))
            return;

        if(pointZero == null)
            barShowing = prevTop >= el.scrollTop;
        else{
            dragging = true;
            let offset = pointZero - el.scrollTop;
            top = topOnHold + offset;
            top = Math.max(Math.min(top, 0), -56);
            barShowing = el.scrollTop == 0 || top >= -56 * 0.5;
        }
        prevTop = el.scrollTop;

        m.redraw();
    }, { passive: true, capture: true });

    window.addEventListener('touchend', function(){
        pointZero = null;
        dragging = false;
        m.redraw();
    }, { passive: true });

    return {

        view(vnode){

            if(!dragging)
                top = barShowing ? 0 : -56;

            let classes =
                (barShowing ? '' : '.closed') +
                (dragging ? '.dragging' : '');

            let actions = [];

            let pc = vnode.attrs.prominent && 'prominent';

            if(vnode.attrs.nav)
                var navBtn = m(Button, { icon: 'menu', action: () => globalState.navDrawerOpen = true });

            if(typeof vnode.attrs.back == 'string')
                navBtn = m(Button, { icon: 'arrow_back', action(){
                    globalState.nav = 'up';
                    m.route.set(vnode.attrs.back);
                } });

            if(vnode.attrs.close)
                navBtn = m(Button, { icon: 'close', action:
                    typeof vnode.attrs.close == 'function'
                        ? vnode.attrs.close
                        : () => window.history.back() });

            if(Array.isArray(vnode.attrs.actions))
                for(let btn of vnode.attrs.actions)
                    actions.push(m(Button, btn));
            else if(typeof vnode.attrs.actions == 'object')
                for(let icon in vnode.attrs.actions)
                    actions.push(m(Button, { icon, action: vnode.attrs.actions[icon] }));

            if(typeof vnode.attrs.search == 'function')
                actions.push(m(Button, { icon: 'search', action: () => openSearch = true }));

            let attrs = { style: { marginTop: top + 'px' } };

            globalState.pageTitle = vnode.attrs.title || 'App Bar';

            return m('header.app-bar' + parseClasses(vnode.attrs.class, pc) + classes,
                attrs,
                navBtn,
                m('h1', globalState.pageTitle),
                ...actions,
                typeof vnode.attrs.search == 'function' && m('section.search',
                    { open: openSearch },
                    m(Button, { icon: 'arrow_back', action: () => {
                        bindSearch.search = '';
                        vnode.attrs.search('');
                        openSearch = false;
                    } }),
                    m(TextField, { label: ' ', placeholder: Strings.SearchBarPlaceholder,
                        onUpdate: vnode.attrs.search, throttle: 400, _type: 'search',
                        bind: bindSearch, name: 'search' })
                )
            );
        }

    };

}
