
import m from 'mithril';
import { List } from './list';
import { globalState } from './util';
import './nav-drawer.css';

function navigate(view){
    globalState.nav = 'top';
    setTimeout(() => m.route.set(view), 500);
}

export function NavDrawer(){

    let leftOnHold, left, dragging = false, leftMin, pointZero, bgOpacity = 0, holdingStill;

    function onclick(ev){
        if(ev.path[0].tagName == 'LI')
            setTimeout(() => globalState.navDrawerOpen = false, 300);

        let { top, left, right, bottom } = ev.target.getBoundingClientRect();
        if(ev.clientX < left || ev.clientX > right || ev.clientY < top
        || ev.clientY > bottom)
            globalState.navDrawerOpen = false;
    }

    return {
        oninit(){
            leftMin = -(document.body.clientWidth - 56);
            left = leftMin;
        },

        view(vnode){

            leftMin = -(document.body.clientWidth - 56);

            if(!dragging)
                left = globalState.navDrawerOpen ? 0 : leftMin;

            bgOpacity = (1 - left / leftMin) * 0.8;

            let classes =
                (globalState.navDrawerOpen ? '' : '.closed') +
                (dragging ? '.dragging' : '');

            for(let i of vnode.attrs.items)
                if(i.text && i.text == globalState.pageTitle){
                    i.classes = '.active';
                    break;
                }

            return m('nav.nav-drawer' + classes,
                {
                    style: { '--bg-opacity': bgOpacity, left: left + 'px' },
                    onclick
                },
                vnode.attrs.header && m('header', vnode.attrs.header),
                m(List, { items: vnode.attrs.items.map(i => {
                    i.view && (i.action = navigate.bind(null, i.view));
                    return i;
                }) } )
            );
        },

        oncreate(vnode){

            vnode.dom.addEventListener('touchstart', function(ev){
                pointZero = ev.touches[0].clientX;
                holdingStill = true;
                leftOnHold = globalState.navDrawerOpen ? 0 : leftMin;
                setTimeout(function(){
                    if(holdingStill && left < 0){
                        left = leftMin + 16;
                        dragging = true;
                        m.redraw();
                    }
                }, 500);
                m.redraw();
            }, { passive: true });

            vnode.dom.addEventListener('touchmove', function(ev){
                dragging = true;
                holdingStill = false;

                let offset = pointZero - ev.touches[0].clientX;
                left = leftOnHold - offset;
                left = Math.max(Math.min(left, 0), leftMin);

                globalState.navDrawerOpen = left >= leftMin * 0.5;

                m.redraw();
            }, { passive: true });

            vnode.dom.addEventListener('touchend', function(){
                holdingStill = false;
                dragging = false;
                m.redraw();
            }, { passive: true });
        },

    };

}
