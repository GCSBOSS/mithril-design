
import m from 'mithril';
import { parseClasses, filterAttrs, globalState, delay, animate, clickRipple } from './util';
import { menu } from './menu';
import './button.css';

export function Button(){

    let action, blocking;
    let menuContent = {};

    async function onclick(ev){
        if(blocking){
            globalState.blockingLoader = true;
            m.redraw();
        }

        clickRipple(this, ev);
        await delay(140);

        let prom = typeof action == 'function' && action(ev);
        await prom;
        if(blocking)
            globalState.blockingLoader = false;
        m.redraw();
    }

    function openMenu(ev){
        menu(menuContent);
        ev.stopPropagation();
    }

    return {

        onupdate(vnode){
            if(menuContent){
                let { left, top } = vnode.dom.getBoundingClientRect();
                menuContent.left = Math.round(left);
                menuContent.top = Math.round(top) - 8;
            }
        },

        view(vnode){

            let type = '';
            if(vnode.attrs.type in { text: 1, contained: 1 })
                type = '.' + vnode.attrs.type;

            if(typeof vnode.attrs.icon == 'string')
                var icon = vnode.attrs.icon;

            action = vnode.attrs.action;
            if(vnode.attrs.menu){
                menuContent.items = vnode.attrs.menu;
                action = openMenu;
            }

            let battrs = filterAttrs(vnode.attrs, 'disabled');
            blocking = vnode.attrs.blocking;

            if(vnode.attrs._type)
                battrs.type = vnode.attrs._type;

            return m(
                'button[type=button]' + type + parseClasses(vnode.attrs.classes),
                {
                    ...battrs, onclick,
                    [vnode.attrs.text ? 'data-icon-pre' : 'data-icon']: icon
                },
                vnode.attrs.text || (icon ? '': 'Button'),
                m('span.effects')
            );
        }

    };
}

export function FAB(){

    return {

        oncreate(vnode){
            animate(vnode.dom, [
                { transform: 'scale(0)' },
                { transform: 'scale(1)' }
            ], { duration: 280 });
        },

        async onbeforeremove(vnode){
            await animate(vnode.dom, [
                { transform: 'scale(1)' },
                { transform: 'scale(0)' }
            ], { duration: 120 });
        },

        view(vnode){
            return m(Button, {
                ...vnode.attrs,
                type: 'contained',
                classes: parseClasses(vnode.attrs.classes, 'fab')
            });
        }

    };
}

export async function fab(opts){
    if(globalState.fab){
        globalState.fab = false;
        m.redraw();
        await delay(300);
    }
    if(opts){
        globalState.fab = opts;
        m.redraw();
    }
}
