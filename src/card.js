
import m from 'mithril';
import { parseClasses, clickRipple } from './util';
import './card.css';

export function Card(){

    let action;

    async function onclick(ev){
        if(ev.target.tagName == 'BUTTON')
            return;

        await clickRipple(this, ev);

        action(ev);
        m.redraw();
    }

    return {

        view(vnode){
            let attrs = {};

            if(typeof vnode.attrs.action == 'function'){
                action = vnode.attrs.action;
                attrs['data-action'] = true;
                attrs.onclick = onclick;
            }

            return m('article.card' + parseClasses(vnode.attrs.classes),
                attrs, vnode.children);
        }

    };

}
