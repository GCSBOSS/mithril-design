
import m from 'mithril';
import { globalState, animate } from './util';
// import { Button } from './button';
import './snack-bar.css';

globalState.snackQueue = [];

export async function pullSnack(){
    let snackOpts = globalState.snackQueue[0];
    if(snackOpts.delay)
        await new Promise(done => setTimeout(done, snackOpts.delay * 1e3));

    globalState.snackBar = snackOpts;
    m.redraw();

    await new Promise(done => setTimeout(done, 4e3));
    globalState.snackBar = false;
    m.redraw();

    await new Promise(done => setTimeout(done, 1e3));
    globalState.snackQueue.shift();

    if(globalState.snackQueue.length > 0)
        pullSnack();
}

export function snackBar(text, opts = {}){
    globalState.snackQueue.push({ ...opts, text });
    if(globalState.snackQueue.length == 1)
        pullSnack();
}

export function SnackBar(){

    return {

        oncreate(vnode){
            animate(vnode.dom,
                vnode.dom.parentNode.childNodes.length == 1
                    ? [ { marginBottom: '-50px' }, { marginBottom: '0' } ]
                    : [ { opacity: 0 }, { opacity: 1 } ],
                { duration: 300 });
        },

        view(vnode){
            return m('aside.snack-bar', vnode.attrs.text);
        },

        async onbeforeremove(vnode){
            await animate(vnode.dom,
                vnode.dom.parentNode.childNodes.length == 1
                    ? [ { marginBottom: '0' }, { marginBottom: '-50px' } ]
                    : [ { opacity: 1 }, { opacity: 0 } ],
                { duration: 200 });
        }
    };
}
