
import m from 'mithril';
import { parseClasses, filterAttrs } from './util';
import Strings from './strings';
import { LinearProgress } from './progress';
import './form.css';

function required(value) {
    let t = typeof value;
    let empty = t === 'string' && value.trim() === '';
    if(empty || t === 'undefined' || value === null)
        return new Error(Strings.RequiredFieldMessage);
}

export function Form(){

    let formModel = {};
    let outerAction = null;
    let validation = {};
    let fields, loading = false;

    function action(ev){
        let ok = true;
        for(let name in fields)
            for(let fn of validation[name]){
                let r = fn(formModel.data[name]);
                if(r instanceof Error){
                    formModel.errors[name] = r.message;
                    ok = false;
                    break;
                }
            }

        if(!ok)
            return;

        if(typeof outerAction != 'function')
            return;

        let p = outerAction(ev);
        if(p instanceof Promise){
            loading = true;
            p.catch(err => {
                loading = false;
                throw err;
            }).then(() => loading = false);
        }
    }

    function onsubmit(){
        return false;
    }

    return {

        view(vnode){
            if(vnode.attrs.bind)
                formModel = vnode.attrs.bind;
            formModel.errors = formModel.errors || {};
            formModel.data = formModel.data || {};

            fields = vnode.attrs.fields;

            let disableSubmit = false;

            for(let name in fields){
                let fvn = fields[name];
                fvn.attrs.name = name;
                fvn.attrs.bind = formModel.data;
                fvn.attrs.bindError = formModel.errors;

                // TOOD other validations

                validation[name] = [];
                if(fvn.attrs.req)
                    validation[name].push(required);

                let vs = [].concat(fvn.attrs.validation);
                vs.forEach(fn => fn && validation[name].push(v => {
                    let r = fn(v);
                    return r ? new Error(r) : r;
                }));

                if(formModel.errors[name])
                    disableSubmit = true;
            }

            let submitBtnAttrs = vnode.attrs.controls.submit.attrs;
            outerAction = submitBtnAttrs.action;
            submitBtnAttrs.action = action;
            submitBtnAttrs.disabled = disableSubmit;
            submitBtnAttrs._type = 'submit';

            let attrs = filterAttrs(vnode.attrs, 'id');

            return m('form' + parseClasses(vnode.attrs.classes, loading ? 'loading' : ''), { ...attrs, onsubmit },
                loading && m(LinearProgress),
                vnode.children
            );
        }

    };

}
