
import m from 'mithril';
import { parseClasses, globalState, fadeIn, fadeOut } from './util';
import { Button } from './button';
import './dialog.css';

globalState.dialogOptions = false;

export function Dialog(){

    return {

        oncreate(vnode){
            fadeIn(vnode);
        },

        async onbeforeremove(vnode){
            await fadeOut(vnode);
        },

        view(vnode){

            let btns = vnode.attrs.buttons || [];

            return m('article.dialog' + parseClasses(vnode.attrs.classes),
                vnode.attrs.title && m('h3', vnode.attrs.title),
                vnode.attrs.text && m('p', vnode.attrs.text),
                btns.length > 0 &&
                    m('footer', btns.map(btn => m(Button, { ...btn, classes: parseClasses(btn.classes, 'text') })))
            );
        }

    };
}

export function closeDialog(){
    globalState.dialogOptions = false;
    m.redraw();
}

export function dialog(options){
    globalState.dialogOptions = options;
    m.redraw();
}

export function confirm(opts){
    return new Promise(done => {
        dialog({
            ...opts,
            buttons: [
                { classes: opts.noClass, text: opts.no || 'Cancel', action: () => {
                    closeDialog();
                    done(false);
                } },
                { classes: opts.yesClass, text: opts.yes || 'OK', action: () => {
                    closeDialog();
                    done(true);
                } }
            ]
        });
    });
}
