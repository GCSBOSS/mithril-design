
import m from 'mithril';
import { parseClasses, filterAttrs } from './util';
import { Button } from './button';
import Strings from './strings';
import './text-field.css';

export function TextField(){

    let name, hostForm = {}, errorForm = {}, dtto;

    return {

        oncreate(vnode){
            vnode.dom.control.addEventListener('input', function(){
                hostForm[name] = vnode.dom.control.value;
                errorForm[name] = null;

                clearTimeout(dtto);
                if(typeof vnode.attrs.onUpdate == 'function')
                    if(vnode.attrs.throttle)
                        dtto = setTimeout(() =>
                            vnode.attrs.onUpdate(hostForm[name]), vnode.attrs.throttle);
                    else
                        vnode.attrs.onUpdate(hostForm[name]);

                m.redraw();
            });

            vnode.dom.control.addEventListener('keydown', function(ev){
                if(typeof vnode.attrs._onKeyDown == 'function')
                    vnode.attrs._onKeyDown(vnode, ev);
            });

            vnode.dom.control.addEventListener('change', function(){
                if(vnode.attrs.req && hostForm[name] === ''){
                    errorForm[name] = new Error(Strings.RequiredFieldMessage);
                    m.redraw();
                }
            });
        },

        view(vnode){
            let req = vnode.attrs.req ? '*' : '';

            if(typeof vnode.attrs.bindError == 'object')
                errorForm = vnode.attrs.bindError;

            let error = errorForm[name] instanceof Error
                ? errorForm[name].message
                : errorForm[name];

            let attrs = {};

            name = vnode.attrs.name || 'text';
            attrs.name = name;

            // TODO maybe function to move stuff to dataset.
            vnode.attrs.prefix && (attrs['data-prefix'] = vnode.attrs.prefix);
            vnode.attrs.suffix && (attrs['data-suffix'] = vnode.attrs.suffix);
            vnode.attrs.id && (attrs.id = vnode.attrs.id);

            let dc = vnode.attrs.disabled ? 'disabled' : '';
            let oc = vnode.attrs.outlined ? 'outline' : '';
            let ec = error ? 'error' : '';

            if(typeof vnode.attrs.bind == 'object')
                hostForm = vnode.attrs.bind;
            typeof hostForm[name] != 'string' && (hostForm[name] = '');


            let iattrs = filterAttrs(vnode.attrs, 'placeholder', 'inputmode',
                'autocomplete', 'onkeydown', 'name', 'disabled', 'oninput',
                'onkeyup', 'readonly', 'onclick', 'onfocus', 'onfocusout');

            iattrs.type = vnode.attrs._type || 'text';
            iattrs.value = hostForm[name];
            iattrs.placeholder = iattrs.placeholder || ' ';

            return m('label.text-field' + parseClasses(vnode.attrs.classes, dc, oc, ec),
                attrs,

                // Leading Icon
                vnode.attrs.icon && m('span.icon', vnode.attrs.icon),

                // Helper & Error
                (vnode.attrs.helper || error)
                    && m('span.helper', error || vnode.attrs.helper),

                // Input
                m('input', iattrs),

                typeof vnode.attrs._children == 'function' && vnode.attrs._children(),

                // Label
                m('span', (vnode.attrs.label || 'Text Field') + req)
            );
        }

    };
}

export function RedactedField(){

    let show = false;

    return {

        view(vnode){

            let opts = { ...vnode.attrs };

            delete opts.prefix;
            delete opts.suffix;
            opts._type = show ? 'text' : 'password';

            opts._children = () => m(Button, {
                icon: show ? 'visibility_off' :'visibility',
                action: e => { show = !show; e.preventDefault(); }
            });

            return m(TextField, opts);
        }

    };
}

export function NumericField(){
    let numbers = '0';
    let masked;
    let outsideForm = {};
    let insideForm = {};

    function _onKeyDown(vnode, ev){
        let mlen = masked.length;

        if(ev.target.selectionStart < mlen){
            ev.target.setSelectionRange(mlen, mlen);
            return ev.preventDefault();
        }

        // TODO support minus key

        if(ev.key == 'Backspace')
            numbers = String(Number(numbers.slice(0, -1)));
        else if(ev.key.length !== 1)
            return;
        else if('0123456789'.indexOf(ev.key) >= 0 && numbers.length < 11)
            numbers = String(Number(numbers + ev.key));
        else
            return ev.preventDefault();

        if(vnode.attrs.bindError)
            vnode.attrs.bindError[vnode.attrs.name] = null;

        numbers == '' && (numbers = '0');

        ev.preventDefault();
        m.redraw();
    }

    return {

        oninit(vnode){
            let decimals = vnode.attrs.decimals || 0;
            let fract = Math.pow(10, decimals);

            if(typeof vnode.attrs.bind == 'object')
                var v = vnode.attrs.bind[vnode.attrs.name || ''];
            if(typeof v == 'number')
                numbers = String(v * fract).replace(/\D/, '');
        },

        view(vnode){
            let decimals = vnode.attrs.decimals || 0;

            if(vnode.attrs.bind)
                outsideForm = vnode.attrs.bind;

            let classes = parseClasses(vnode.attrs.classes, 'numeric');

            let fract = Math.pow(10, decimals);
            let numeric = Number(numbers) / fract;

            if(vnode.attrs.max && numeric > vnode.attrs.max){
                numbers = String(vnode.attrs.max * fract);
                numeric = vnode.attrs.max;
            }
            else if(vnode.attrs.min && numeric < vnode.attrs.min){
                numbers = String(vnode.attrs.min * fract);
                numeric = vnode.attrs.min;
            }

            masked = new Intl.NumberFormat(undefined, {
                minimumFractionDigits: decimals,
                maximumFractionDigits: decimals
            }).format(numeric);

            let name = vnode.attrs.name || '';

            insideForm[name] = masked;
            outsideForm[name] = numeric;

            return m(TextField, { ...vnode.attrs, classes, bind: insideForm,
                onclick: ev => ev.target.setSelectionRange(masked.length, masked.length),
                _onKeyDown, name, inputmode: 'decimal' });
        }

    }
}

export function TimeField(){
    let outsideForm = {};
    let insideForm = {};

    return {

        view(vnode){
            if(vnode.attrs.bind)
                outsideForm = vnode.attrs.bind;

            let classes = parseClasses(vnode.attrs.classes, 'time');

            let name = vnode.attrs.name || '';
            let tt = vnode.attrs.timestamp;

            let dt = new Date(insideForm[name]);
            outsideForm[name] = isNaN(dt)
                ? null
                : tt ? dt.getTime() : dt;

            return m(TextField, { ...vnode.attrs, _type: 'datetime-local', bind: insideForm,
                classes,
                name });
        }

    }
}
