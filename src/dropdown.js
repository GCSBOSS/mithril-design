
import m from 'mithril';
import { filterAttrs, isNullish, parseClasses } from './util';
import { TextField } from './text-field';
import { menu } from './menu';
import { Button } from './button';
import './dropdown.css';

function parseItems(vnode, key = 'items'){

    let rowId = vnode.attrs.rowId || 'id';
    let rowName = vnode.attrs.rowName || 'name';

    let dataset = { rows: [], index: {}, id: rowId, name: rowName, items: [] };
    let input = vnode.attrs[key];

    if(Array.isArray(input))
        for(let i of input){
            let obj = typeof i == 'object' ? i : { [rowId]: i, [rowName]: i };
            dataset.rows.push(obj);
            dataset.index[obj[rowId]] = obj;
            dataset.items.push({ text: obj[rowName], value: obj[rowId] });
        }
    else if(typeof input == 'object')
        for(let k in input){
            let obj = typeof input[k] == 'object'
                ? { [rowId]: k, ...input[k] }
                : { [rowId]: k, [rowName]: input[k] };
            dataset.rows.push(obj);
            dataset.items.push({ text: obj[rowName], value: k });
            dataset.index[k] = obj;
        }

    return dataset;
}

export function Dropdown(){

    let hostForm = {};
    let internalForm = {};
    let name = '';
    let overriding = false;
    let number = false;
    let dataset = {};
    let attrs = {};
    let menuPos = {};

    // TODO Edit (Free)
    // TODO Filter (Local)
    // TODO Filter Fetch

    function openMenu(ev){
        menu({
            items: dataset.items,
            classes: '.dropdown',
            placeholder: dataset.rows.length == 0 && 'No items were found',
            action: function(value){
                value = number ? Number(value) : value;
                let text = dataset.index[value][attrs.rowName];
                hostForm[name] = value;
                internalForm[name] = text;
                typeof attrs.onSelect == 'function' && attrs.onSelect(value, text);
            },
            ...menuPos
        });
        ev.stopPropagation();
    }

    let onfocus = openMenu;
    let onclick = ev => ev.stopPropagation();
    let onfocusout = () => menu(false);

    function onUpdate(value){
        overriding = value !== '';
        hostForm[name] = number ? Number(value) : value;
        internalForm[name] = value;
    }

    return {

        onupdate(vnode){
            let { height, left, top, width } = vnode.dom.getBoundingClientRect();
            menuPos.left = Math.round(left);
            menuPos.top = Math.round(top + height) - 8;
            menuPos.width = Math.floor(width);
        },

        view(vnode){
            vnode.attrs.items = vnode.attrs.items || [ 'Item 1', 'Item 2', 'Item 3' ];

            dataset = parseItems(vnode);
            attrs = vnode.attrs;

            let iattrs = filterAttrs(vnode.attrs, 'placeholder', 'req',
                'disabled', 'label');

            name = vnode.attrs.name || 'item';

            if(typeof vnode.attrs.bind == 'object')
                hostForm = vnode.attrs.bind;

            if(!isNullish(hostForm[name]) && !overriding)
                internalForm[name] = dataset.index[hostForm[name]]
                    ? dataset.index[hostForm[name]][vnode.attrs.rowName || 'name']
                    : '';

            let classes = parseClasses('dropdown', vnode.attrs.classes);

            iattrs.readonly = true;

            number = vnode.attrs.number || false;

            return m(TextField, {
                label: 'Dropdown', ...iattrs, onUpdate, onclick,
                classes, onfocus, onfocusout, bind: internalForm, name,
                _children: () => m(Button, { icon: 'arrow_drop_down' })
            });


        }

    };
}
