import m from 'mithril';
import { standardEasing } from './view';

export function parseClasses(...specs){
    let r = '';
    for(let s of specs)
        if(Array.isArray(s))
            r += parseClasses(...s);
        else if(typeof s == 'string')
            r += '.' + s;
    return r;
}

export function delay(ms){
    return new Promise(done => setTimeout(done, ms));
}

export function normalize(input){
    return Array.isArray(input) ? input : [input];
}

function getAttrFromKey(key){
    return key.replace(/[A-Z]/g, m => '-' + m.toLowerCase());
}

export function clickRipple(dom, ev){
    let bound = Math.max(dom.clientWidth, dom.clientHeight) * 3.14;
    dom.style.setProperty('--ripple-size', String(bound) + 'px');
    dom.style.setProperty('--ripple-x', String(ev.offsetX - bound / 2) + 'px');
    dom.style.setProperty('--ripple-y', String(ev.offsetY - bound / 2) + 'px');

    return animate(dom, [
        {},
        { transform: 'scale(1)' }
    ], { duration: 400, pseudoElement: '::after' }).then(() =>
        animate(dom, [
            { transform: 'scale(1)' },
            { opacity: 0, transform: 'scale(1)' }
        ], { duration: 800, pseudoElement: '::after' }));
}

export function filterAttrs(source, ...forward){
    let r = {};

    source.id && (r.id = source.id);

    if(typeof source.dataset == 'object'){
        for(let k in source.dataset)
            r['data-' + getAttrFromKey(k)] = source.dataset[k];
        delete source.dataset;
    }

    for(let k of forward)
        if(k in source)
            r[getAttrFromKey(k)] = source[k];

    return r;
}

export function animate(dom, kfs, opts){
    let a = dom.animate(kfs, { easing: standardEasing, ...opts });
    return new Promise(done => a.onfinish = done);
}

export function isNullish(value){
    return typeof value == 'undefined' || value === null;
}

export var globalState = {};

export function fadeIn(vnode){
    animate(vnode.dom, [ { transform: 'scale(0.8)' }, { } ],
        { duration: 150, easing: 'cubic-bezier(0.0, 0.0, 0.2, 1)' })
    animate(vnode.dom, [ { opacity: 0 }, { opacity: 1 } ],
        { duration: 45, easing: 'linear' });
    animate(vnode.dom, [ { '--scrim-color': '#0000' }, {  } ],
        { duration: 150, easing: 'linear' });
}

export async function fadeOut(vnode){
    await animate(vnode.dom, [
        { opacity: 1 },
        { opacity: 0 }
    ], { duration: 75, easing: 'linear' });
}

export async function app(opts){
    opts = opts || {};
    let views = opts.views || {};

    if(opts.title)
        globalState.appTitle = opts.title;

    if(opts.nav)
        globalState.navDrawerComponent = opts.nav;

    m.route.prefix = '';

    let routes = {};
    for(let path in views)
        routes[path] = views[path];

    if(opts.notFound)
        routes['/:notFound...'] = opts.notFound;

    await new Promise(done => window.addEventListener('load', done));
    //await new Promise(done => document.onreadystatechange = done);

    // TODO wait for fonts to fully load

    if(opts.worker)
        globalState.worker = await navigator.serviceWorker.register(opts.worker);

    document.body.classList.add('app');

    m.route(document.body, opts.home || '/', routes);
}
